#! /usr/bin/env python3

import argparse
import collections
import itertools
import os
import sys
import cuptlib


parser = argparse.ArgumentParser(description="""
        Example code that reads CUPT file and prints first 3 sentences.""")
parser.add_argument("input_file", type=argparse.FileType('r'),
        help="""The input file in .cupt format""")



class Main(object):
    def __init__(self, args):
        sys.excepthook = cuptlib.excepthook
        self.args = args


    def run(self):
        n_sentences = 0

        for sentence in cuptlib.iter_cupt_sentences(self.args.input_file):

            # Print some information taken from the tokens
            sentence_text = " ".join(w['FORM'] for w in sentence.tokens)
            print('==> SENTENCE:', sentence_text)
            print('==> FIRST WORD (LEMMA):', repr(sentence.tokens[0]['LEMMA']))
            print('==> FIRST WORD (UPOS):', repr(sentence.tokens[0]['UPOS']))

            # We add an MWE for tokens [0,2,5] with category FakeCategory
            sentence.add_mwes({(0,2,5): 'FakeCategory'})

            # For each MWE in sentence, print some information
            for mwe_id, mweinfo in sentence.mwe_infos().items():
                mwe_text = " ".join(w['FORM'] for w in mweinfo.tokens)
                print('==> MWE #{}: {}, AT INDEXES {} (CATEGORY: {})'.format(
                    mwe_id, repr(mwe_text), mweinfo.token_indexes, mweinfo.category))

            print('--------------')

            # Print the sentence in CUPT format
            print(sentence.to_cupt())
            print()

            n_sentences += 1
            if n_sentences == 3:
                print("##> Stopping after 3 sentences")
                exit(0)



#####################################################

if __name__ == "__main__":
    Main(parser.parse_args()).run()
