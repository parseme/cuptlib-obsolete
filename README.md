# Example
The script `example_usage.py` contains an example of how to use the `cuptlib`.

# Testing
You can test your script using `sample_corpus.cupt`. For example:

```bash
./example_usage.py sample_corpus.cupt
```
